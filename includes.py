import db

def create_war(server_id, with_teams, teams, server_roles):
    id_ww = db.add_war(server_id, with_teams)
    ww_teams = []

    if with_teams and teams:
        # On itère sur les roles du serveur pour récupérer les infos des teams de la WW
        # (oui c'est sale mais je peux pas faire plus propre que ce que la lib autorise, n'est-ce pas)
        for role in server_roles:
            if role.name in teams:
                role_id = role.id
                ww_teams.append((str(id_ww), role_id.strip()))

    if id_ww and with_teams:
        db.add_teams_to_ww(id_ww, ww_teams)

    if not id_ww:
        return "error_creating_ww"

def create_battle(server_id, battle_name, start, end):
    return db.create_battle(server_id, battle_name, start, end)

def update_wc(server_id, author_id, teams_id, add_wc, add, wc_type='word_war'):
    return db.update_personnal_ww_wc(server_id, author_id, teams_id, add_wc, add, wc_type)

# def update_battle_wc(server_id, author_id, teams_id, add_wc, add):
#     return db.update_my_battle_wc(server_id, author_id, teams_id, add_wc, add)

def get_teams_wc(server_id):
    result = db.get_teams_wc(server_id)
    teams = {}

    for res in result:
        if res['id_team'] not in teams.keys():
            teams[res['id_team']] = {'team_id': res['id_team'],'total': res['word_count']}
        else:
            teams[res['id_team']]['total']+= res['word_count']

        if 'team_members' not in teams[res['id_team']].keys():
            teams[res['id_team']]['team_members'] = {res['id_member']: res['word_count']}
        else:
            teams[res['id_team']]['team_members'][res['id_member']] = res['word_count']

    return teams

def get_ww_ranking(server_id):
    ranking = db.get_ww_ranking(server_id)
    return ranking

def get_server_ranking(server_id):
    ranking = db.get_server_ranking(server_id)
    return ranking

def get_my_wc(server_id, author_id):
    result = db.get_my_wc(server_id, author_id)
    return result